# Google Lacuna

A lacuna is a missing portion in a manuscript. This repository is the missing
portion of Google Open Source projects (in C++) that make the various other
Google repositories work well together. All the code is adapted from other
Google Open Source projects; see the section _Sources_ for specific information.

The following resources are included in this project:

- A modern version of [glog](https://github.com/google/glog), Google's logging
  library.
  - Use flags to control logging; see
    [log_flags.cpp](src/absl/logging/log_flags.cpp).
- Helpful utilities for working with `absl::Status` types:
  - The `StatusBuilder` class which helps create and modify status objects.
  - The macros `RETURN_IF_ERROR` and `ASSIGN_OR_RETURN` which can be used to
    forward status codes in functions.
- Test utilities for `googletest`:
  - `ASSERT_OK` and `EXPECT_OK` for ensuring a function returns an OK status.
  - The `StatusIs` matcher, which provides detailed assertions about status
    codes for use in `ASSERT_THAT` and `EXPECT_THAT`.

## Compilation

This project uses the [Meson](https://mesonbuild.com/) build system. To build
the code, use the following commands:

``` shell
meson setup build/
ninja -C build/ all
```

I'm using `gcc-10` to take advantage of the latest C++20 standards.

## Project Libraries

The following libraries are provided as a part of the `google-lacuna`
repository:

- `gl_init_dep`
- `gl_absl_base_dep`
- `gl_absl_debugging_dep`
- `gl_absl_logging_dep`
- `gl_absl_status_dep`
- `gl_gtest_dep`

Additionally, `google-lacuna` forwards the dependencies of the libraries it
includes. Below is an example snippet which includes all provided libraries
from `google-lacuna`:

```meson
gl_proj = subproject('google-lacuna')
gl_init_dep = gl_proj.get_variable('gl_init_dep')
gl_absl_base_dep = gl_proj.get_variable('gl_absl_base_dep')
absl_container_dep = gl_proj.get_variable('absl_container_dep')
absl_cord_dep = gl_proj.get_variable('absl_cord_dep')
gl_absl_debugging_dep = gl_proj.get_variable('gl_absl_debugging_dep')
absl_flags_dep = gl_proj.get_variable('absl_flags_dep')
absl_hash_dep = gl_proj.get_variable('absl_hash_dep')
gl_absl_logging_dep = gl_proj.get_variable('gl_absl_logging_dep')
absl_numeric_dep = gl_proj.get_variable('absl_numeric_dep')
absl_random_dep = gl_proj.get_variable('absl_random_dep')
gl_absl_status_dep = gl_proj.get_variable('gl_absl_status_dep')
absl_strings_dep = gl_proj.get_variable('absl_strings_dep')
absl_synchronization_dep = gl_proj.get_variable('absl_synchronization_dep')
absl_time_dep = gl_proj.get_variable('absl_time_dep')
absl_types_dep = gl_proj.get_variable('absl_types_dep')

protobuf_lite_dep = gl_proj.get_variable('protobuf_lite_dep')
protobuf_dep = gl_proj.get_variable('protobuf_dep')
protoc_gen = gl_proj.get_variable('protoc_gen')

gl_gtest_dep = gl_proj.get_variable('gl_gtest_dep')
gtest_dep = gl_proj.get_variable('gtest_dep')
gtest_main_dep = gl_proj.get_variable('gtest_main_dep')
gmock_dep = gl_proj.get_variable('gmock_dep')
gmock_main_dep = gl_proj.get_variable('gmock_main_dep')
```

In some cases, `google-lacuna` wraps and masks a library, providing new sources
together with the old sources. The only exception to this is `gl_gtest_dep`,
which is designed to be used along with `gtest_dep`.

## Sources

Each file contains a reference to its original source as a comment in the header
file. Most of the code in this repository is adapted from
[google/xls](https://github.com/google/xls).
