#include "init/init_google_lacuna.h"

#include <filesystem>
#include <fstream>
#include <string_view>
#include <vector>

#include "absl/flags/flag.h"
#include "absl/flags/parse.h"
#include "absl/flags/usage.h"
#include "absl/logging/log_flags.h"
#include "absl/logging/logging.h"
#include "absl/strings/str_join.h"
#include "absl/time/time.h"

namespace google_lacuna {

class FileLogSink : public absl::LogSink {
 public:
  explicit FileLogSink(std::string_view path) {
    output.open(path.data(), std::ios_base::app);
  }

  void Send(const absl::LogEntry& entry) override {
    output << absl::StrCat(entry.ToString(), "\n");
    output.flush();
  }

  bool ok() const { return output.good(); }

 private:
  std::ofstream output;
};

static FileLogSink* sink = nullptr;

void InitLogging(int argc, char** argv) {
  if (!absl::GetFlag(FLAGS_logtostderr)) {
    const char* slash = strrchr(argv[0], '/');
    const char* short_program_name = slash ? slash + 1 : argv[0];

    absl::Time now = absl::Now();
    absl::TimeZone utc = absl::UTCTimeZone();
    std::string logfile_name =
        absl::StrJoin({short_program_name, "log",
                       absl::FormatTime("%Y%m%d-%H%M%S", now, utc).c_str()},
                      ".");
    std::string logfile_path =
        absl::StrJoin({"/tmp", logfile_name.c_str()}, "/");

    sink = new FileLogSink(logfile_path);
    CHECK(sink->ok()) << "Failed to create log sink";
    absl::AddLogSink(sink);
  }
}

}  // namespace google_lacuna

std::vector<std::string_view> InitGoogleLacuna(std::string_view usage, int argc,
                                               char** argv) {
  absl::SetProgramUsageMessage(usage);
  std::vector<char*> positional_args = absl::ParseCommandLine(argc, argv);
  google_lacuna::InitLogging(argc, argv);
  return std::vector<std::string_view>(positional_args.begin(),
                                       positional_args.end());
}
