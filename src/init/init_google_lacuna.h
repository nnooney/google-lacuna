#ifndef INIT_GOOGLE_LACUNA_H_
#define INIT_GOOGLE_LACUNA_H_

#include <string_view>
#include <vector>

// Initializes common binary utilities, including logging and flag processing.
// Returns the positional arguments passed at the command line (optional
// arguments are assigned to Abseil flag variables).
std::vector<std::string_view> InitGoogleLacuna(std::string_view usage, int argc,
                                               char** argv);

#endif  // INIT_GOOGLE_LACUNA_H_
